/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/main/java/domain/Entity.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.domain;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.TemporalType.DATE;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.google.common.base.Objects;
import com.jaxio.jpa.querybyexample.Identifiable;

@Entity
@Table(name = "`TRANSACTION`")
public class Transaction implements Identifiable<Integer>, Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(Transaction.class.getName());

    // Raw attributes
    private Integer id;
    private BigDecimal amount;
    private Date transactionDate;
    private Date valueDate;
    private String description;
    private Integer version;

    // Many to one
    private Currency currency;
    private Account account;

    @Override
    public String entityClassName() {
        return Transaction.class.getSimpleName();
    }

    // -- [id] ------------------------

    @Override
    @Column(name = "ID", precision = 10)
    @GeneratedValue
    @Id
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Transaction id(Integer id) {
        setId(id);
        return this;
    }

    @Override
    @Transient
    @XmlTransient
    public boolean isIdSet() {
        return id != null;
    }

    // -- [amount] ------------------------

    @Digits(integer = 18, fraction = 2)
    @NotNull
    @Column(name = "AMOUNT", nullable = false, precision = 20, scale = 2)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Transaction amount(BigDecimal amount) {
        setAmount(amount);
        return this;
    }

    // -- [transactionDate] ------------------------

    @NotNull
    @Column(name = "TRANSACTION_DATE", nullable = false, length = 8)
    @Temporal(DATE)
    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Transaction transactionDate(Date transactionDate) {
        setTransactionDate(transactionDate);
        return this;
    }

    // -- [valueDate] ------------------------

    @NotNull
    @Column(name = "VALUE_DATE", nullable = false, length = 8)
    @Temporal(DATE)
    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public Transaction valueDate(Date valueDate) {
        setValueDate(valueDate);
        return this;
    }

    // -- [description] ------------------------

    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Transaction description(String description) {
        setDescription(description);
        return this;
    }

    // -- [version] ------------------------

    @Column(name = "VERSION", precision = 10)
    @Version
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Transaction version(Integer version) {
        setVersion(version);
        return this;
    }

    // -----------------------------------------------------------------
    // Many to One support
    // -----------------------------------------------------------------

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // many-to-one: Transaction.currency ==> Currency.id
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @NotNull
    @JoinColumn(name = "CURRENCY_ID", nullable = false)
    @ManyToOne(cascade = { PERSIST, MERGE }, fetch = LAZY)
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Set the {@link #currency} without adding this Transaction instance on the passed {@link #currency}
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Transaction currency(Currency currency) {
        setCurrency(currency);
        return this;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // many-to-one: Transaction.account ==> Account.id
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @JoinColumn(name = "ACCOUNT_ID", nullable = false)
    @ManyToOne(cascade = { PERSIST, MERGE }, fetch = LAZY)
    public Account getAccount() {
        return account;
    }

    /**
     * Set the {@link #account} without adding this Transaction instance on the passed {@link #account}
     * If you want to preserve referential integrity we recommend to use
     * instead the corresponding adder method provided by {@link Account}
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    public Transaction account(Account account) {
        setAccount(account);
        return this;
    }

    /**
     * Apply the default values.
     */
    public Transaction withDefaults() {
        return this;
    }

    /**
     * Equals implementation using a business key.
     */
    @Override
    public boolean equals(Object other) {
        return this == other || (other instanceof Transaction && hashCode() == other.hashCode());
    }

    private IdentifiableHashBuilder identifiableHashBuilder = new IdentifiableHashBuilder();

    @Override
    public int hashCode() {
        return identifiableHashBuilder.hash(log, this);
    }

    /**
     * Construct a readable string representation for this Transaction instance.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return Objects.toStringHelper(this) //
                .add("id", getId()) //
                .add("amount", getAmount()) //
                .add("transactionDate", getTransactionDate()) //
                .add("valueDate", getValueDate()) //
                .add("description", getDescription()) //
                .add("version", getVersion()) //
                .toString();
    }
}