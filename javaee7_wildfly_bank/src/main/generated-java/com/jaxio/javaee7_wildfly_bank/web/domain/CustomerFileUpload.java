/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/domain/FileUpload.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.domain;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.jaxio.javaee7_wildfly_bank.domain.Customer;

public class CustomerFileUpload {
    private Customer customer;

    public CustomerFileUpload(Customer customer) {
        this.customer = customer;
    }

    /**
     * Primefaces support for contractBinary file upload
     */
    public void onContractBinaryFileUpload(FileUploadEvent fileUploadEvent) {
        UploadedFile uploadedFile = fileUploadEvent.getFile(); //application code
        customer.setContractBinary(uploadedFile.getContents());
        customer.setContractSize(customer.getContractBinary().length);
        customer.setContractContentType(uploadedFile.getContentType());
        customer.setContractFileName(FilenameUtils.getName(uploadedFile.getFileName()));
    }
}