/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/domain/Controller.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.domain;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jaxio.javaee7_wildfly_bank.domain.Currency;
import com.jaxio.javaee7_wildfly_bank.printer.CurrencyPrinter;
import com.jaxio.javaee7_wildfly_bank.repository.CurrencyRepository;
import com.jaxio.javaee7_wildfly_bank.web.domain.support.GenericController;
import com.jaxio.javaee7_wildfly_bank.web.permission.CurrencyPermission;

/**
 * Stateless controller for {@link Currency}. 
 */
@ApplicationScoped
@Named
public class CurrencyController extends GenericController<Currency, Integer> {
    public static final String CURRENCY_EDIT_URI = "/domain/currencyEdit.faces";
    public static final String CURRENCY_SELECT_URI = "/domain/currencySelect.faces";

    @Inject
    protected CurrencyRepository currencyRepository;
    @Inject
    protected CurrencyPermission currencyPermission;
    @Inject
    protected CurrencyPrinter currencyPrinter;

    @PostConstruct
    public void init() {
        init(currencyRepository, currencyPermission, currencyPrinter, CURRENCY_SELECT_URI, CURRENCY_EDIT_URI);
    }
}