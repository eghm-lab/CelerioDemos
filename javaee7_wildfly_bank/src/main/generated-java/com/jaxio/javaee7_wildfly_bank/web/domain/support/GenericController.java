/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/domain/support/GenericController.p.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.domain.support;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang.StringUtils.isBlank;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.WordUtils;

import com.google.common.base.Splitter;
import com.jaxio.javaee7_wildfly_bank.printer.support.GenericPrinter;
import com.jaxio.javaee7_wildfly_bank.web.permission.support.GenericPermission;
import com.jaxio.javaee7_wildfly_bank.web.util.MessageUtil;
import com.jaxio.jpa.querybyexample.GenericRepository;
import com.jaxio.jpa.querybyexample.Identifiable;
import com.jaxio.jpa.querybyexample.JpaUniqueUtil;
import com.jaxio.jpa.querybyexample.MetamodelUtil;
import com.jaxio.jpa.querybyexample.SearchParameters;
import com.jaxio.jpa.querybyexample.TermSelector;

/**
 * Base controller for JPA entities providing helper methods to:
 * <ul>
 *  <li>navigate to select/edit view</li>
 *  <li>support autoComplete component</li>
 *  <li>perform actions</li>
 *  <li>support excel export</li>
 * </ul>
 */
public abstract class GenericController<E extends Identifiable<PK>, PK extends Serializable> {
    private static final String PERMISSION_DENIED = "/error/accessdenied";
    private String selectUri;
    private String editUri;

    private transient Logger log = Logger.getLogger(GenericController.class.getName());
    @Inject
    protected JpaUniqueUtil jpaUniqueUtil;
    @Inject
    protected MessageUtil messageUtil;
    @Inject
    protected MetamodelUtil metamodelUtil;
    protected GenericRepository<E, PK> repository;
    protected GenericPermission<E> permission;
    protected GenericPrinter<E> printer;

    public void init(GenericRepository<E, PK> repository, GenericPermission<E> permission, GenericPrinter<E> printer, String selectUri, String editUri) {
        this.repository = checkNotNull(repository);
        this.permission = checkNotNull(permission);
        this.printer = checkNotNull(printer);
        this.selectUri = checkNotNull(selectUri);
        this.editUri = checkNotNull(editUri);
    }

    public GenericRepository<E, PK> getRepository() {
        return repository;
    }

    public PK convertToPrimaryKey(String pkAsString) {
        return repository.convertToPrimaryKey(pkAsString);
    }

    public GenericPermission<E> getPermission() {
        return permission;
    }

    public MessageUtil getMessageUtil() {
        return messageUtil;
    }

    public String select() {
        checkPermission(permission.canSelect());
        return selectUri + "?faces-redirect=true";
    }

    public String create() {
        checkPermission(permission.canCreate());
        return editUri + "?faces-redirect=true";
    }

    public String edit(E entity) {
        checkPermission(permission.canEdit(entity));
        return editUri + "?faces-redirect=true&id=" + entity.getId().toString();
    }

    public String view(E entity) {
        checkPermission(permission.canView(entity));
        return editUri + "?faces-redirect=true&readonly=true&id=" + entity.getId().toString();
    }

    public String print(E entity) {
        checkPermission(permission.canView(entity));
        return editUri + "?faces-redirect=true&readonly=true&print=true&id=" + entity.getId().toString();
    }

    protected void checkPermission(boolean check) {
        if (!check) {
            throw new IllegalStateException();
        }
    }

    // ----------------------------------------
    // AUTO COMPLETE SUPPORT  
    // ----------------------------------------

    /**
     * Auto-complete support. This method is used by primefaces autoComplete component.
     */
    public List<E> complete(String value) {
        try {
            SearchParameters searchParameters = new SearchParameters() //
                    .limitBroadSearch() //
                    .distinct() //
                    .orMode();
            E template = repository.getNew();
            for (String property : completeProperties()) {
                if (repository.isIndexed(property)) {
                    searchParameters.addTerm(new TermSelector(metamodelUtil.toAttribute(property, repository.getType())).selected(value));
                } else {
                    PropertyUtils.setProperty(template, property, value);
                }
            }
            return repository.find(template, searchParameters);
        } catch (Exception e) {
            log.warning("error during complete: " + e.getMessage());
            throw propagate(e);
        }
    }

    protected Iterable<String> completeProperties() {
        String completeOnProperties = parameter("completeOnProperties", String.class);
        return isBlank(completeOnProperties) ? printer.getDisplayedAttributes() : Splitter.on(";,").omitEmptyStrings().split(completeOnProperties);
    }

    public List<String> completeProperty(String value) {
        return completeProperty(value, parameter("property", String.class), parameter("maxResults", Integer.class));
    }

    public List<String> completeProperty(String value, String property) {
        return completeProperty(value, property, null);
    }

    public List<String> completeProperty(String toMatch, String property, Integer maxResults) {
        List<String> values = newArrayList();
        if (repository.isIndexed(property)) {
            values.addAll(completePropertyUsingFullText(toMatch, property, maxResults));
        } else {
            values.addAll(completePropertyInDatabase(toMatch, property, maxResults));
        }
        if (isBlank(toMatch) || values.contains(toMatch)) {
            // the term is already in the results, return them directly
            return values;
        } else {
            // add the term before the results as it is not part of the results
            List<String> retWithValue = newArrayList(toMatch);
            retWithValue.addAll(values);
            return retWithValue;
        }
    }

    protected List<String> completePropertyUsingFullText(String term, String property, Integer maxResults) {
        try {
            SearchParameters searchParameters = new SearchParameters().limitBroadSearch().distinct();
            searchParameters.addTerm(new TermSelector(metamodelUtil.toAttribute(property, repository.getType())).selected(term));
            if (maxResults != null) {
                searchParameters.setMaxResults(maxResults);
            }
            return repository.findProperty(String.class, repository.getNew(), searchParameters, property);
        } catch (Exception e) {
            log.warning("error during completePropertyUsingFullText: " + e.getMessage());
            throw propagate(e);
        }
    }

    protected List<String> completePropertyInDatabase(String value, String property, Integer maxResults) {
        try {
            SearchParameters searchParameters = new SearchParameters() //
                    .limitBroadSearch() //
                    .caseInsensitive() //
                    .anywhere() //
                    .distinct();
            if (maxResults != null) {
                searchParameters.setMaxResults(maxResults);
            }
            E template = repository.getNew();
            PropertyUtils.setProperty(template, property, value);
            return repository.findProperty(String.class, template, searchParameters, property);
        } catch (Exception e) {
            log.warning("error during completePropertyInDatabase: " + e.getMessage());
            throw propagate(e);
        }
    }

    /**
     * A simple autoComplete that returns exactly the input. It is used in search forms with {@link PropertySelector}.
     */
    public List<String> completeSame(String value) {
        return newArrayList(value);
    }

    @SuppressWarnings("unchecked")
    protected <T> T parameter(String propertyName, Class<T> expectedType) {
        return (T) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get(propertyName);
    }

    protected SearchParameters defaultOrder(SearchParameters searchParameters) {
        return searchParameters;
    }
}