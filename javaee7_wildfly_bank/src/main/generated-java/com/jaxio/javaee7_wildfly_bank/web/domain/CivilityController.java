/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/domain/EnumController.enum.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.domain;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.jaxio.javaee7_wildfly_bank.domain.Civility;
import com.jaxio.javaee7_wildfly_bank.web.domain.support.GenericEnumController;

@ApplicationScoped
@Named
public class CivilityController extends GenericEnumController<Civility> {

    @PostConstruct
    public void init() {
        init(Civility.values());
    }
}
