/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/domain/EditForm.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.domain;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jaxio.javaee7_wildfly_bank.domain.Account;
import com.jaxio.javaee7_wildfly_bank.domain.Address;
import com.jaxio.javaee7_wildfly_bank.domain.Customer;
import com.jaxio.javaee7_wildfly_bank.repository.CustomerRepository;
import com.jaxio.javaee7_wildfly_bank.web.domain.support.GenericEditForm;
import com.jaxio.javaee7_wildfly_bank.web.domain.support.GenericToManyAssociation;
import com.jaxio.javaee7_wildfly_bank.web.domain.support.GenericToOneAssociation;
import com.jaxio.javaee7_wildfly_bank.web.util.TabBean;

/**
 * View Helper/Controller to edit {@link Customer}.
 */
@ViewScoped
@Named
public class CustomerEditForm extends GenericEditForm<Customer, Integer> {
    private static final long serialVersionUID = 1L;
    @Inject
    protected transient CustomerController customerController;
    @Inject
    protected transient AddressController addressController;
    protected transient GenericToOneAssociation<Address, Integer> address;
    @Inject
    protected transient AccountController accountController;
    protected transient GenericToManyAssociation<Account, Integer> accounts;
    protected TabBean tabBean = new TabBean();

    public CustomerEditForm() {
        // mandatory no-args constructor to make this bean proxyable
    }

    @Inject
    public CustomerEditForm(CustomerRepository customerRepository, CustomerGraphLoader customerGraphLoader) {
        super(customerRepository, customerGraphLoader);
    }

    /**
     * View helper to store the x-to-many associations tab's index. 
     */
    @Override
    public TabBean getTabBean() {
        return tabBean;
    }

    /**
     * The entity to edit/view.
     */
    public Customer getCustomer() {
        return getEntity();
    }

    @Override
    protected void onInit() {
        setupAddressActions();
        setupAccountsActions();
    }

    public String print() {
        return customerController.print(getCustomer());
    }

    void setupAddressActions() {
        address = new GenericToOneAssociation<Address, Integer>("customer_address", addressController) {
            @Override
            protected Address get() {
                return getCustomer().getAddress();
            }

            @Override
            protected void set(Address address) {
                getCustomer().setAddress(address);
            }
        };
    }

    public GenericToOneAssociation<Address, Integer> getAddress() {
        return address;
    }

    void setupAccountsActions() {
        accounts = new GenericToManyAssociation<Account, Integer>(getCustomer().getAccounts(), "customer_accounts", accountController) {
            @Override
            protected void remove(Account account) {
                getCustomer().removeAccount(account);
            }

            @Override
            protected String extraCreateIds() {
                return getCustomer().isIdSet() ? "&customerId=" + getCustomer().getId().toString() : null;
            }
        };
    }

    public GenericToManyAssociation<Account, Integer> getAccounts() {
        return accounts;
    }
}