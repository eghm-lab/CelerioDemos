/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-frontend:src/main/java/filter/DelegatingFilter.p.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.deltaspike.core.api.provider.BeanProvider;

public class DelegatingFilter implements Filter {
    private Filter innerFilter;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String filterBeanName = filterConfig.getFilterName();
        innerFilter = BeanProvider.getContextualReference(filterBeanName, false, Filter.class);
        innerFilter.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        innerFilter.doFilter(request, response, chain);
    }

    @Override
    public void destroy() {
        innerFilter.destroy();
    }
}