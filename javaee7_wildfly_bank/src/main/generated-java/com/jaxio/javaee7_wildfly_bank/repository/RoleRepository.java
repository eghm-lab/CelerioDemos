/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/main/java/repository/Repository.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.jaxio.javaee7_wildfly_bank.domain.Role;
import com.jaxio.jpa.querybyexample.GenericRepository;

/**
 * {@link GenericRepository} for {@link Role} 
 */
@ApplicationScoped
@Named
public class RoleRepository extends GenericRepository<Role, Integer> {

    public RoleRepository() {
        super(Role.class, Integer::valueOf);
    }

    @Override
    public Role getNew() {
        return new Role();
    }

    @Override
    public Role getNewWithDefaults() {
        return getNew().withDefaults();
    }

    /**
     * Return the persistent instance of {@link Role} with the given unique property value roleName,
     * or null if there is no such persistent instance.
     *
     * @param roleName the unique value
     * @return the corresponding {@link Role} persistent instance or null
     */
    @Transactional
    public Role getByRoleName(String roleName) {
        return findUniqueOrNone(new Role().roleName(roleName));
    }

    /**
     * Delete a {@link Role} using the unique column roleName
     *
     * @param roleName the unique value
     */
    @Transactional
    public void deleteByRoleName(String roleName) {
        delete(getByRoleName(roleName));
    }
}