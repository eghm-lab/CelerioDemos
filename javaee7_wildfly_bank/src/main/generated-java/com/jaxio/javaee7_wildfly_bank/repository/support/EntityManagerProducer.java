/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/main/java/repository/support/EntityManagerProducer.p.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.repository.support;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Annotated with {@link Startup} to force initialization of entity meta-models at startup before use of meta-models by other beans.
 */
@Named
@ApplicationScoped
@Startup
public class EntityManagerProducer {
    @PersistenceContext(unitName = "javaee7_wildfly_bankPU")
    private EntityManager entityManager;

    @Produces
    @RequestScoped
    @Default
    protected EntityManager createEntityManager() {
        return entityManager;
    }
}
