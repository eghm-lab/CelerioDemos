/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/main/java/repository/Repository.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.jaxio.javaee7_wildfly_bank.domain.Account;
import com.jaxio.jpa.querybyexample.GenericRepository;

/**
 * {@link GenericRepository} for {@link Account} 
 */
@ApplicationScoped
@Named
public class AccountRepository extends GenericRepository<Account, Integer> {

    public AccountRepository() {
        super(Account.class, Integer::valueOf);
    }

    @Override
    public Account getNew() {
        return new Account();
    }

    @Override
    public Account getNewWithDefaults() {
        return getNew().withDefaults();
    }

    /**
     * Return the persistent instance of {@link Account} with the given unique property value accountNumber,
     * or null if there is no such persistent instance.
     *
     * @param accountNumber the unique value
     * @return the corresponding {@link Account} persistent instance or null
     */
    @Transactional
    public Account getByAccountNumber(String accountNumber) {
        return findUniqueOrNone(new Account().accountNumber(accountNumber));
    }

    /**
     * Delete a {@link Account} using the unique column accountNumber
     *
     * @param accountNumber the unique value
     */
    @Transactional
    public void deleteByAccountNumber(String accountNumber) {
        delete(getByAccountNumber(accountNumber));
    }
}