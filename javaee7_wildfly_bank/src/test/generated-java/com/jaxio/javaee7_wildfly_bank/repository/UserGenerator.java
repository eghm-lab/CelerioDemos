/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/test/java/service/ModelGenerator.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.repository;

import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jaxio.javaee7_wildfly_bank.domain.Civility;
import com.jaxio.javaee7_wildfly_bank.domain.User;
import com.jaxio.javaee7_wildfly_bank.util.ValueGenerator;

/**
 * Helper class to create transient entities instance for testing purposes.
 * Simple properties are pre-filled with random values.
 */
@Named
@Singleton
public class UserGenerator {

    /**
     * Returns a new User instance filled with random values.
     */
    public User getUser() {
        User user = new User();

        // simple attributes follows
        user.setUsername(ValueGenerator.getUniqueString(100));
        user.setPassword("a");
        user.setEmail("dummy@dummy.com");
        user.setIsEnabled(true);
        user.setCivility(Civility.MR);
        user.setFirstName("a");
        user.setLastName("a");
        user.setCreationDate(new Date());
        user.setCreationAuthor("a");
        user.setLastModificationDate(new Date());
        user.setLastModificationAuthor("a");
        return user;
    }

}