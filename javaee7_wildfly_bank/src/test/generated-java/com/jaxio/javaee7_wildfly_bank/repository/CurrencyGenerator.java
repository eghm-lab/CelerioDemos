/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-javaee7-backend:src/test/java/service/ModelGenerator.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/javaee-lab
 */
package com.jaxio.javaee7_wildfly_bank.repository;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jaxio.javaee7_wildfly_bank.domain.Currency;

/**
 * Helper class to create transient entities instance for testing purposes.
 * Simple properties are pre-filled with random values.
 */
@Named
@Singleton
public class CurrencyGenerator {

    /**
     * Returns a new Currency instance filled with random values.
     */
    public Currency getCurrency() {
        Currency currency = new Currency();

        // simple attributes follows
        currency.setCode("ddd");
        currency.setName("a");
        currency.setDecimalCount(1);
        return currency;
    }

}