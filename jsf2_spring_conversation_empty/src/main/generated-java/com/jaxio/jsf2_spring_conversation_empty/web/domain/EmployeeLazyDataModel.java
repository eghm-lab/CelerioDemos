/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-jsf2-spring-conversation:src/main/java/domain/LazyDataModel.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-jsf2-spring-conversation
 */
package com.jaxio.jsf2_spring_conversation_empty.web.domain;

import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import com.jaxio.jsf2_spring_conversation_empty.domain.Employee;
import com.jaxio.jsf2_spring_conversation_empty.repository.EmployeeRepository;
import com.jaxio.jsf2_spring_conversation_empty.web.domain.support.GenericLazyDataModel;
import com.jaxio.jsf2_spring_conversation_empty.web.faces.ConversationContextScoped;

/**
 * Provide PrimeFaces {@link LazyDataModel} for {@link Employee}
 */
@Named
@ConversationContextScoped
public class EmployeeLazyDataModel extends GenericLazyDataModel<Employee, Integer, EmployeeSearchForm> {
    private static final long serialVersionUID = 1L;

    @Inject
    public EmployeeLazyDataModel(EmployeeRepository employeeRepository, EmployeeController employeeController, EmployeeSearchForm employeeSearchForm,
            EmployeeExcelExporter employeeExcelExporter) {
        super(employeeRepository, employeeController, employeeSearchForm, employeeExcelExporter);
    }
}