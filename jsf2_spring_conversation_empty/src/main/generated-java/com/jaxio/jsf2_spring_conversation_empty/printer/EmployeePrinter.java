/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/main/java/printer/Printer.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.jaxio.jsf2_spring_conversation_empty.printer;

import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jaxio.jsf2_spring_conversation_empty.domain.Employee;
import com.jaxio.jsf2_spring_conversation_empty.domain.Employee_;
import com.jaxio.jsf2_spring_conversation_empty.printer.support.GenericPrinter;

/**
 * {@link GenericPrinter} for {@link Employee} 
 *
 * @see GenericPrinter
 * @see TypeAwarePrinter
 */
@Named
@Singleton
public class EmployeePrinter extends GenericPrinter<Employee> {
    public EmployeePrinter() {
        super(Employee.class, Employee_.empuhnumber, Employee_.empfirstname, Employee_.emplastname);
    }

    @Override
    public String print(Employee employee, Locale locale) {
        if (employee == null) {
            return "";
        }
        StringBuilder ret = new StringBuilder();
        appendIfNotEmpty(ret, employee.getEmpuhnumber());
        appendIfNotEmpty(ret, employee.getEmpfirstname());
        appendIfNotEmpty(ret, employee.getEmplastname());
        return ret.toString();
    }
}