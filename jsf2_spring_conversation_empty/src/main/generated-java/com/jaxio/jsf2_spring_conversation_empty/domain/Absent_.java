/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/main/java/domain/EntityMeta_.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.jaxio.jsf2_spring_conversation_empty.domain;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Absent.class)
public abstract class Absent_ {

    // Raw attributes
    public static volatile SingularAttribute<Absent, Integer> id;
    public static volatile SingularAttribute<Absent, Date> absdate;
    public static volatile SingularAttribute<Absent, String> abstype;
    public static volatile SingularAttribute<Absent, String> absnotes;
    public static volatile SingularAttribute<Absent, Boolean> absdeleted;

    // Many to one
    public static volatile SingularAttribute<Absent, Employee> emp;
}