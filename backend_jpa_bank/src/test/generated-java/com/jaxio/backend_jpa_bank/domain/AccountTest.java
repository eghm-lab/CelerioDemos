/*
 * Source code generated by Celerio, a Jaxio product.
 * Documentation: http://www.jaxio.com/documentation/celerio/
 * Follow us on twitter: @jaxiosoft
 * Need commercial support ? Contact us: info@jaxio.com
 * Template pack-backend-jpa:src/test/java/domain/ModelTest.e.vm.java
 * Template is part of Open Source Project: https://github.com/jaxio/pack-backend-jpa
 */
package com.jaxio.backend_jpa_bank.domain;

import static org.fest.assertions.Assertions.assertThat;

import java.io.*;
import java.util.*;

import org.junit.Test;

import com.jaxio.backend_jpa_bank.util.ValueGenerator;

/**
 * Basic tests for Account
 */
@SuppressWarnings("unused")
public class AccountTest {

    // test unique primary key
    @Test
    public void newInstanceHasNoPrimaryKey() {
        Account model = new Account();
        assertThat(model.isIdSet()).isFalse();
    }

    @Test
    public void isIdSetReturnsTrue() {
        Account model = new Account();
        model.setId(ValueGenerator.getUniqueInteger());
        assertThat(model.getId()).isNotNull();
        assertThat(model.isIdSet()).isTrue();
    }

    //-------------------------------------------------------------
    // Many to One:  Account.currency ==> Currency.id
    //-------------------------------------------------------------

    @Test
    public void manyToOne_setCurrency() {
        Account many = new Account();

        // init
        Currency one = new Currency();
        one.setId(ValueGenerator.getUniqueInteger());
        many.setCurrency(one);

        // make sure it is propagated properly
        assertThat(many.getCurrency()).isEqualTo(one);

        // now set it to back to null
        many.setCurrency(null);

        // make sure null is propagated properly
        assertThat(many.getCurrency()).isNull();
    }

    @Test
    public void manyToOne_setCustomer() {
        Account many = new Account();

        // init
        Customer one = new Customer();
        one.setId(ValueGenerator.getUniqueInteger());
        many.setCustomer(one);

        // make sure it is propagated properly
        assertThat(many.getCustomer()).isEqualTo(one);

        // now set it to back to null
        many.setCustomer(null);

        // make sure null is propagated properly
        assertThat(many.getCustomer()).isNull();
    }

    //-------------------------------------------------------------
    // One to Many: SimpleOneToMany ACCOUNT.ID ==> TRANSACTION.ACCOUNT_ID
    //-------------------------------------------------------------

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // transaction.transaction <-- account.accounts
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    @Test
    public void oneToMany_addTransaction() {
        Account one = new Account();
        Transaction many = new Transaction();

        // init
        one.addTransaction(many);

        // make sure it is propagated
        assertThat(one.getTransactions()).contains(many);
        assertThat(one).isEqualTo(many.getAccount());

        // now set it to null
        one.removeTransaction(many);

        // make sure null is propagated
        assertThat(one.getTransactions().contains(many)).isFalse();
        assertThat(many.getAccount()).isNull();
    }

    @Test
    public void equalsUsingBusinessKey() {
        Account model1 = new Account();
        Account model2 = new Account();
        String accountNumber = ValueGenerator.getUniqueString(100);
        model1.setAccountNumber(accountNumber);
        model2.setAccountNumber(accountNumber);
        assertThat(model1).isEqualTo(model2);
        assertThat(model2).isEqualTo(model1);
        assertThat(model1.hashCode()).isEqualTo(model2.hashCode());
    }
}